# Description #

Kalman-FOAM is a library based on OpenFOAM® technology, extending the capabilties of OpenFOAM in the area of Data Driven for computational fluid dynamics. The aim of the library is an integration of 
the Kalman filter within segragate methods: PISO, PIMPLE. In addition, a mass-conservation properties is enforced. 

The results of application is shown in the paper:

A mass conservative Kalman filter algorithm for thermo-computational fluid dynamics, Carolina Introini, Stefano Lorenzi, Antonio Baroli Davide, Cammi, Benhard Peters, S.P.A Bordas. Submitted.

Such implentation allows to integrate the data within the fluid dynamics application of OpenFOAM.

# Dependencies #

Code is compiled against OpenFOAM 3.2 and foam-extend 3.2.

# Description of files#

The results presented in the paper can be reproduced running
	- KalmanPISOFoam which perform mass conservative Kalman Filter within  PISO segregated method given a velocity data 
	- KalmanPIMPLEFoam   which perform mass conservative Kalman Filter within  PIMPLE segregated method given a velocity data
	- BoussinesqPISOKalmanFoam which perform mass conservative Kalman Filter within  PIMPLE segregated method given a velocity data and/or temperature data
	
# Notes #

The experimental data should be compatible with OpenFOAM field file, and provided in folder namely with time instant when data occurs.
In this implementation, the lenght of experimental data should be of the same number of grid elements, assign zero where data-location is not available.

# Issues and Support#

For support or questions please email carolina.introini@polimi.it and davide.baroli@uni.lu.

# Authors #
Carolina Introini, Politecnico di Milano
Davide Baroli, University of Luxembourg
Stefano Lorenzi, Politecnico di Milano

# Licence #
Kalman-Foam is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License along with Kalman-Foam. If not, see http://www.gnu.org/licenses/.

# Disclaimer #
This offering is not approved or endorsed by OpenCFD Limited, producer and
distributor of the OpenFOAM software via www.openfoam.com, and owner of the
OPENFOAM®  and OpenCFD® trade marks.





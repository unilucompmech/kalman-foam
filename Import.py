
# - Import NumPy module (provides arrays operation and random number generation)
# - Import the transition format CSV (OpenFOAM <-> CSV <-> Python)
import numpy 
import csv

# - Does not work with the matrix containing the vectors of velocity - only scalar components
# - Import the measure matrix from the case folder into the specified variable
MM_X = numpy.genfromtxt("MeasureMatrixX.txt")
MM_Y = numpy.genfromtxt("MeasureMatrixY.txt")
MM_Z = numpy.genfromtxt("MeasureMatrixZ.txt")
MM_P = numpy.genfromtxt("MeasureMatrixP.txt")
MM_T = numpy.genfromtxt("MeasureMatrixT.txt")

# - Get number of rows (number of internal points) and number of columns (number of snapshots)
NR = MM_P.shape[0]
NC = MM_P.shape[1]

# - Text files are directly read by Python. Modify POD routines to have as output already formatted text files

# - Now working with standard OpenFOAM files (with OpenFOAM formatting)

# - Save the first snapshot into an array
UX_CSV = MM_X[:,NC-1]
P_CSV = MM_P[:,NC-1]

# - Add noise to create a perturbed solution
Mean = numpy.zeros(NR)
Std = numpy.zeros(NR)

for I in range (0,NR):
  Mean[I] = numpy.mean(MM_P[I,:])
  Std[I] = numpy.var(MM_P[I,:])
Variance = numpy.mean(Std);

V = open('MeasureVariance','w')
V.write('1 1\n(\n')
V.write("("+str(Variance)+")")
V.write("\n)")
V.close()

# - Variance: take the variance of data per row (same position, different times)

P_CSV = P_CSV + Variance

# - Create file where to write the Snapshots (Velocity)
F = open('Write.csv','w')
Header = str(NR)+" 1"
F.write(Header)                       # - Write the required header
F.write("\n(")                          # - Write the required first line

# - For cycle to write all the elements of U0_CSV
for I in range(0,len(U0_CSV)):
  F.write("\n("+str(U0_CSV[I])+")")
  
F.write("\n)")
F.close()

# - Create file where to write the P snapshots
FP = open('WriteP.csv','w')
Header = str(NR)+" 1"
FP.write(Header)                       # - Write the required header
FP.write("\n(")                          # - Write the required first line

# - For cycle to write all the elements of U0_CSV
for I in range(0,len(P_CSV)):
  FP.write("\n("+str(P_CSV[I])+")")
  
FP.write("\n)")
FP.close()

#--------------------------------------------------------------------------------

# - Uniform Perturbation on Complete Solution

import chaospy
import numpy
MM_X = numpy.genfromtxt("MeasureMatrixX.txt")
MM_Y = numpy.genfromtxt("MeasureMatrixY.txt")
MM_Z = numpy.genfromtxt("MeasureMatrixZ.txt")
MM_T = numpy.genfromtxt("MeasureMatrixT.txt")
NR = MM_X.shape[0]
SigmaU = 0#0.00001
SigmaT = 0#0.00001
DistributionU = chaospy.Normal(0, 0.00001)
DistributionT = chaospy.Normal(0, 0.00001)
#SigmaU = DistributionU.sample(size=NR,rule="L")
#SigmaT = DistributionT.sample(size=NR,rule="L")
UX = MM_X + SigmaU
UY = MM_Y + SigmaU
UZ = MM_Z + SigmaU
T = MM_T + SigmaT
FU = open('Measure.csv','w')
FT = open('TMeasure.csv','w')
Header = str(NR)+" 1"
FU.write(Header)                       
FT.write(Header)
FU.write("\n(")    
FT.write("\n(")                      
# - For cycle to write all the elements of U0_CSV
for I in range(0,len(UX)):
  FU.write("\n( ("+str(UX[I])+" "+str(UY[I])+" "+str(UZ[I])+") )")
  #FU.write("\n( ("+str(UX[I])+" "+str(UY[I])+" "+str(UZ)+") )")
  FT.write("\n( "+str(T[I])+" )")
FU.write("\n)")
FT.write("\n)")
FU.close()
FT.close()


#-------------------------------------------------------------------------------------

# - Non Uniform perturbed solution (perturbation = Gaussian white noise)
# - Distribution = Normal (0, 0.001)

import chaospy
import numpy
MM_Z = 0;
MM_X = numpy.genfromtxt("MeasureMatrixX.txt")
MM_Y = numpy.genfromtxt("MeasureMatrixY.txt")
MM_Z = numpy.genfromtxt("MeasureMatrixZ.txt")
NR = MM_X.shape[0]
#Sample = 0.00001
Distribution = chaospy.Normal(0, 0.00001)
Sample = Distribution.sample(size=NR,rule="L")
UX = MM_X + Sample
UY = MM_Y + Sample
UZ = MM_Z + Sample
#import random
#Location = random.sample(range(1, NR),8)
Location = [20, 70, 120, 170, 220, 270, 320, 370]
Flag = numpy.zeros(NR)
NMeasure = len(Location)
for I in range (0,NMeasure):
  Flag[Location[I]] = 1
F = open('Measure.csv','w')
Header = str(NR)+" 1"
F.write(Header)                       
F.write("\n(")                          
# - For cycle to write all the elements of U0_CSV
for I in range(0,len(UX)):
  if (Flag[I]):
    F.write("\n( ("+str(UX[I])+" "+str(UY[I])+" "+str(UZ[I])+") )")
  else:
    F.write("\n( ("+str(0)+" "+str(0)+" "+str(0)+") )")      
    
F.write("\n)")
F.close()

# ------------------------------------------------------------------------------------

import chaospy
import numpy
import random
MM_X = numpy.genfromtxt("MeasureMatrixX.txt")
NR = MM_X.shape[0]
#MeasVarU = numpy.ones(NR)*0.000001
#MeasVarT = numpy.ones(NR)*0.000001
DistributionU = chaospy.Normal(0, 1e-5)
#DistributionT = chaospy.Normal(0, 1e-5)
MeasVarU = DistributionU.sample(size=NR,rule="L")
#MeasVarT = DistributionT.sample(size=NR,rule="L")
for I in range (0, len(MeasVarU)):
  if (MeasVarU[I] < 0):
    MeasVarU[I] = MeasVarU[I]*(-1)
  #if (MeasVarT[I] < 0):
    #MeasVarT[I] = MeasVarT[I]*(-1)   
UX = MM_X
FU = open('MeasureVarianceU','w')
#FT = open('MeasureVarianceT','w')
Header = str(NR)+" 1"
FU.write(Header)                       
#FT.write(Header)
FU.write("\n(")
#FT.write("\n(")                          
for I in range(0,len(UX)):
  FU.write("\n( "+str(MeasVarU[I])+" )")
  #FT.write("\n( "+str(MeasVarT[I])+" )")
FU.write("\n)")
#FT.write("\n)")
FU.close()
#FT.close()




